'''
Python script to plot a spectogram of mouse movements recorded by 'mouse_recorder.py'
For the Hathi-March BioSeismic project (Frugal Science)

	Credits:
		Spectogram sections : https://fairyonice.github.io/implement-the-spectrogram-from-scratch-in-python.html
		
	Team - 
		- Sumithra Suredralal
		- Nish Kothari
		- Cecilia Herbert
		- Subir Bhaduri
	
	updated - 9th Nov. 2020, Subir
'''

import os
from pandas import read_csv
from pandas import datetime
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from scipy import signal
import numpy as np
import sys 
  
# User entries.
#-------------------------------------------------------------------------------

# Data file name and location
filename = 'mousedata_SP1ms.2020-11-09_17-41-00'

# mention the sample time. This could be adjusted to reduce interpolation and be more truer representative of vibrations
sample_period_ms = 20

# Spectogram upper threshold value.
spectogram_upper_threshold = 2


# Parsing the data file into a pandas dataframe!
#-------------------------------------------------------------------------------
df = read_csv( filename, header=None, names=['datetime','X','Y'], dtype=str)
# drop rows containing NaN values
df = df.apply(lambda x: pd.Series(x.dropna().values))
# Convert X and Y to integers.
df['X'] = df['X'].astype(int)
df['Y'] = df['Y'].astype(int)
print(df.head())


# Convert the time column into datetime object. 
df['time'] = pd.to_datetime(df['datetime'], format="%d/%m/%Y %I:%M:%S.%f")

# Make the time column into an index. This is required for sampling and interpolation
df.set_index("time",inplace=True)

# drop the datetimt columns
df.drop('datetime', axis=1, inplace=True)

# Unsample all the data in specified millis seconds sample time. 
df_resampled = df.resample(str(sample_period_ms)+'L').mean()

# print dataframes for testing.
print(df_resampled.head())
#print(df_resampled.head(-10))


# Compute sample rate.
#-------------------------------------------------------------------------------
sample_rate_sps   = 1.0/sample_period_ms*1e3	# set aboive, this corresponds to sampling of 20ms.


# Compute the spectorgrams and plot.
#------------------------------------------------------------------------------- 
f, t, Sxx = signal.spectrogram(df_resampled['Y'], sample_rate_sps)
# Plot
plt.figure('Mouse Spectogram Y')
c = plt.pcolormesh(t, f, Sxx, vmin=Sxx.min(), vmax=spectogram_upper_threshold, cmap='PuBu_r')
plt.colorbar(c)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [min]')
plt.tight_layout()

# Compute the spectorgram. 
f, t, Sxx = signal.spectrogram(df_resampled['X'], sample_rate_sps)
# Plot
plt.figure('Mouse Spectogram X')
c = plt.pcolormesh(t, f, Sxx, vmin=Sxx.min(), vmax=spectogram_upper_threshold, cmap='PuBu_r')
plt.colorbar(c)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [min]')
plt.tight_layout()

# Plot the X and Y values as a line.
plt.figure('Vs time')
plt.plot(df_resampled['X'],color='r')
plt.plot(df_resampled['Y'],color='g')
plt.tight_layout()
plt.show()



