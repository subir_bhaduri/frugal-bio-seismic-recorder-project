# Frugal Bio-seismic recorder project - 'Seismousmeter'

This project concerns an attempt develop a frugal elephant early warning system for small village farmers who's crops are frequently raided by wild elephants.

**Team**:
Cecilia Herbert, Sumithra Surendralal, Nish Kothari, Kishopre P., Subir Bhaduri.

**Team page**:
https://www.notion.so/Hathi-March-Bioacoustic-Sensors-d828cb51cf8f4c5ca51b715001b7cf8c

**Background**: We started as a part of the Frugal Science course (BioE271, https://www.frugalscience.org). Within the discussions on problems of the world which need frugal local solutions, Kishore Panaganti, a researcher of wildlife-human interactions in the State of Karnataka, Southern India, posed a problem of elephants raiding the deliscious crops of small farmers due to shrinking forest habitats. As a consequence, farmers need to spend anxious days patrolling the village-forest borders, chasing elephants and so on. We propsed to develop some frugal means of monitoring elephant populations near the villages, from far distances, so that farmers can take timely actions.

Elephants emit infrasounds and seismic waves for communicating with eachother as well as during walking and foraging activities. These seismic waves can be sensed kilometers away. Could we listen to these seismic signals and alert the farmers in advance to reduce uncertainty of elephant movements in the village vincinity? 

**Proposed solution**: The common computer optical mice can detect minute movements. So the idea is to test if this feature could be used to build a seismic listening and recording device. The idea is to place a sensitive pendulum kind of device at the focal plane of a computer mouse and track mouse coordinates on a computer. We hope that a good combination of high resolution/speed gaming mice and a good seismometer design could lead to an overall effective device for our purpose.

**Gitlab repository files**
1) MRP.py - This 'Mouse recorder and Plotter' python script records the local timestamp and the XY mouse movememnts when run on Windows/Linux computers and stores them in the CSV format. It also plots.
2) spectogram.py - This python script takes in CSV files made by the MRP.py and converts them into a spectogram for visual analysis. 

**Prerequisits**
1) Install python 3 on your system.
2) Install pyqtgraph by typing : "pip install pyqtgraph"
3) Install pandas by typing : "pip install pandas"

**How to use these?** 
1) Just download the above files and store into a directory.
2) For recording mouse movements: *python MRP.py*.
3) For plotting spectogram: 
A) Copy the full name of the new log file created by MRP.py.
B) Paste in spectogram.py
C) Do: *python spectogram.py* 

